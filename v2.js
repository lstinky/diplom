let GGoS_Values = {
	socket : '',
	tmp:'',
	message_Status:[],
	message_Getting:[],
	message_Channel_Count:[],
	message_Get_History:[],
	message_Welcome:undefined,
	message_Success_Join:undefined,
	started_Status:false,
	started_Message:false,
	started_Channel_Count:false,
	started_Get_History:false,
	started_auth:false,
	connected :false,
	reconnected:true,
	joined:false,
	count_send:0,
	frequency:1000,
	data : {},
	auth_data:{}
}


function GGoS(Data){
	GGoS.data = Data.Auth;
	if (GGoS.data.channel)
		GGoS.joined = true;
	if (GGoS.data.login && GGoS.data.pass)
		GGoS.started_auth = true;
};
GGoS.__proto__ = GGoS_Values;



GGoS.prototype.Connect = returned => {
	re_Connect = () => {
		if (!GGoS.connected){
			body_Connect();
			//$('#Work_area').append('<div>Ветка боди боди коннект connnected? :' + GGoS.connected + ' </div>');
			setTimeout(re_Connect,5000);
		}
	}
	re_Connect();
	function body_Connect(){
		GGoS.socket = new WebSocket('wss://chat.goodgame.ru/chat/websocket');
		GGoS.socket.onopen = () =>  {
			GGoS.connected = true;
			GGoS.reconnected = true;
			//$('#Work_area').append('<div>Подключились и изменили соединение на :' + GGoS.connected + ' </div>');
			if (GGoS.started_Status)
				this.message_Status.push({'Type':'Status','Message':'Соединение установлено', 'Code': 0});
			returned('Status', {'Message':'Соединение установлено'}, 0);
			GGoS.prototype.prepear_Connect();
		};
		GGoS.socket.onclose = event =>  {
			GGoS.connected = false;
			//$('#Work_area').append('<div>Отключились и изменили содениенине на ' + GGoS.connected + ' </div>');
			if (GGoS.started_Status){
				if (event.isTrusted) {
					GGoS.message_Status.push({'Type':'Status','Message':'Соединение закрыто чисто', 'Code':1});
				}else{
					GGoS.message_Status.push({'Type':'Status','Message':'Обрыв соединения', 'Code': 2});
				}
				//GGoS.message_Status.push({'Type':'Status','Message':' причина: ' + event.reason, 'Code' : event.code}); // !!! не выводит код и причину !!!  развернутая причина ->{"isTrusted":true}
			}
			returned('Status',{'Message':'Соединение закрыто'}, 1);
			re_Connect();
		};
		GGoS.socket.onerror = error =>  {
			//$('#Work_area').append('<div>В работе сокета возникла ошибка ' + GGoS.connected + ' </div>');
			if (GGoS.started_Status)
				GGoS.message_Status.push({'Type':'Error','Message':'В работе сокета возникла ошибка: ' + error.message, 'Code': 3});
				returned('Error', {'Message':'В работе сокета возникла ошибка'}, 3);
		};
		GGoS.socket.onmessage = event =>  {
			get_data = JSON.parse(event.data);
			returned(get_data.type, get_data.data, 5);
			switch (get_data.type){
				case 'channel_history':
					if (GGoS.started_Get_History){
						let localTemp = GGoS.tmp; // получаем из общего хранилища ?
						var usr = get_data.data.messages; // переприсваиваем сообщения в usr
						if (localTemp == ""){ //если в общем хранилице ыло пусто
							usr.forEach(function(usr){ // перебираем все сообщения дабы выбрать последнее
								GGoS.tmp = usr.message_id; //присваиваем номер сообщения
							}); // на выходе должен храниться номер последнего сообщения
						}else{ // если в хранилище всетаки что-то было
							let prnt = false; // говорим что пока ничего печатать не нужно
							let ksh = ""; // какой то кэш
							usr.forEach(function(usr){ // вновь перебираем все сообщения
								if (prnt == true){ // если печать доступна то помещаем сообщение в очередь на вывод
									GGoS.message_Get_History.push({'User':usr.user_name, 'Message': usr.text, 'Code': 4});
								}
								if (localTemp == usr.message_id)
									prnt = true; // если номер последнего сообщения совпал с текущим считанным то разрешаем печать
								ksh = usr.message_id; // изменяем номер последнего сообщения
							});
							GGoS.tmp = ksh; // записываем в общее хранилище номер последнего сообщения
						}
					}
				break;
				case 'welcome': //4-ку зарезервируем для входящего сообщения
					GGoS.message_Welcome = {'protocolVersion':get_data.data.protocolVersion, 'serverIdent':get_data.data.serverIdent, 'Code' : 6};
				break;
				case 'success_join':
					GGoS.message_Status.push({'Type':'Join', 'Message':JSON.stringify(get_data.data),'Code':7});
					// Информация по подключенному каналу
				break;
				case 'channel_counters':
					if (GGoS.started_Channel_Count)
						GGoS.message_Channel_Count.push({'Data' : get_data.data, 'Code' : 8});
					// Тут можно организовать хранение инфы по количеству людей в чате и номеру канала
				break;
				case 'message':
					//GGoS.message_Get_History.push({'User':get_data.data.user_name, 'Message': get_data.data.text, 'Code': 9});
				break;
				default:
					//$('#Work_area').append('<div>Не типизированные входне данные' + JSON.stringify(get_data) + ' </div>');
			}
			//GGoS.message_Getting.push({'Type' : get_data.type, 'Data' : get_data.data, 'Code' : 4});
		}
		window.onunload = () => {
			GGoS.socket.close();
		}
	}
}


GGoS.prototype.prepear_Connect = () => {
	if (GGoS.started_auth){
		if (undefined == GGoS.auth_data.login && undefined == GGoS.auth_data.pass){
			$.ajax({
				type: 'POST',
				url: 'https://goodgame.ru/ajax/chatlogin',
				data:{
					"login":GGoS.data.login,
					"password":GGoS.data.pass
				},
				success:function(get_ajx_data){
					if (true == get_ajx_data.result){
						GGoS.auth_data.login = get_ajx_data.user_id;
						GGoS.auth_data.pass = get_ajx_data.token;
						GGoS.prototype.send_auth();
					}else{
						GGoS.started_auth = false;
					}
					GGoS.prototype.re_Con();
				}
			});
		}else{
			GGoS.prototype.send_auth();
			GGoS.prototype.re_Con();
		}
	}else{
		GGoS.prototype.re_Con();
	}
}


GGoS.prototype.re_Con = () => {
	if (GGoS.joined)
		GGoS.prototype.join();

}


GGoS.prototype.join = () => {
	let msg = {
		"type": "join",
		"data": {
			"channel_id": GGoS.data.channel, // идентификатор канала
			"hidden": false   // для "модераторов": не показывать ник в списке юзеров
		}
	}
	GGoS.prototype.send_msg(msg);
	delete msg;
}


GGoS.prototype.send_msg = msg => {
	GGoS.count_send++;
	if (GGoS.connected){
		GGoS.count_send = 0
		GGoS.socket.send(JSON.stringify(msg));
	}else{
		if (GGoS.count_send < 5){
			setTimeout(function(){
				GGoS.prototype.send_msg(msg)
			},500);
		}
	}
}


GGoS.prototype.send_auth = () => {
	let msg = {
		"type": "auth",
		"data": {
			"user_id": GGoS.auth_data.login,
			"token": GGoS.auth_data.pass
		}
	}
	GGoS.prototype.send_msg(msg)
	delete msg;
	//$('#Work_area').append('<div> Отправили запрос на авторизацию</div>');
}

GGoS.prototype.send_message = text => {
	if (GGoS.started_auth){
		let msg = {
			"type": "send_message",
			"data": {
				"channel_id": GGoS.data.channel,
				"text": text,
				"hideIcon": false,
				"mobile": false
			}
		}
		GGoS.prototype.send_msg(msg);
		delete msg;
	}
}
